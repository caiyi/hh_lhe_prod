import ROOT
import numpy as np
import sys

def safe_fill(hist, value, weight):
    min_bin_center = hist.GetBinCenter(1)
    max_bin_center = hist.GetBinCenter(hist.GetNbinsX())
    if value < min_bin_center:
        value = min_bin_center
    elif value > max_bin_center:
        value = max_bin_center
    hist.Fill(value,weight)


inputs = {"-1":      "/afs/cern.ch/user/c/caiyi/eos/HH4b/production/upload/vali/000006/DAOD_TRUTH1.test.pool.root",
          "0":       "/afs/cern.ch/user/c/caiyi/eos/HH4b/production/upload/vali/000001/DAOD_TRUTH1.test.pool.root",
          "1":       "/afs/cern.ch/user/c/caiyi/eos/HH4b/production/upload/vali/000002/DAOD_TRUTH1.test.pool.root",
          "2.5":     "/afs/cern.ch/user/c/caiyi/eos/HH4b/production/upload/vali/000005/DAOD_TRUTH1.test.pool.root",
          "5":       "/afs/cern.ch/user/c/caiyi/eos/HH4b/production/upload/vali/000004/DAOD_TRUTH1.test.pool.root",
          "10":      "/afs/cern.ch/user/c/caiyi/eos/HH4b/production/upload/vali/000003/DAOD_TRUTH1.test.pool.root",
          "-1_mc20": "/afs/cern.ch/user/c/caiyi/eos/HH4b/production/upload/vali_mc20/000006/DAOD_TRUTH1.test.pool.root",
          "0_mc20":  "/afs/cern.ch/user/c/caiyi/eos/HH4b/production/upload/vali_mc20/000001/DAOD_TRUTH1.test.pool.root",
          "1_mc20":  "/afs/cern.ch/user/c/caiyi/eos/HH4b/production/upload/vali_mc20/000002/DAOD_TRUTH1.test.pool.root",
          "2.5_mc20":"/afs/cern.ch/user/c/caiyi/eos/HH4b/production/upload/vali_mc20/000005/DAOD_TRUTH1.test.pool.root",
          "5_mc20":  "/afs/cern.ch/user/c/caiyi/eos/HH4b/production/upload/vali_mc20/000004/DAOD_TRUTH1.test.pool.root",
          "10_mc20": "/afs/cern.ch/user/c/cawiyi/eos/HH4b/production/upload/vali_mc20/000003/DAOD_TRUTH1.test.pool.root",
          "1_mc16":   "/afs/cern.ch/user/c/caiyi/eos/HH4b/production/comp/mc16_13TeV.600463.PhPy8EG_PDF4LHC15_HH4b_cHHH01d0.merge.EVNT.e8222_e7400/DAOD_TRUTH1.test.DAOD.root",
          "10_mc16":  "/afs/cern.ch/user/c/caiyi/eos/HH4b/production/comp/mc16_13TeV.600464.PhPy8EG_PDF4LHC15_HH4b_cHHH10d0.merge.EVNT.e8222_e7400/DAOD_TRUTH1.test.DAOD.root",
          "1_mc21":  "/eos/user/l/lshi/Shared/hh4b_mc21/TRUTH1/kl1/DAOD_TRUTH1.PhPyHH.pool.root",
          "10_mc21": "/eos/user/l/lshi/Shared/hh4b_mc21/TRUTH1/kl10/DAOD_TRUTH1.PhPyHH.pool.root",}
group = list(inputs.keys())
nkl = int(sys.argv[1]) if len(sys.argv) > 1 else 0
kl = group[nkl]
print(kl)

variables = {"mhh"     : {'label': r"$m_{HH}\ \text{[GeV]}$", 'range': np.linspace(200, 1000, 41)},
             "mbbbb"   : {'label': r"$m_{bbbb}\ \text{[GeV]}$", 'range': np.linspace(200, 1000, 41)},
             "mbb1"    : {'label': r"$m_{\text{(bb from H1)}}\ \text{[GeV]}$", 'range': np.linspace(0, 200, 41)},
             "mbb2"    : {'label': r"$m_{\text{(bb from H2)}}\ \text{[GeV]}$", 'range': np.linspace(0, 200, 41)},
             
             "ptbb1"   : {'label': r"$p_{T}\text{(bb from H1)\ [GeV]}$", 'range': np.linspace(0, 500, 41)},
             "ptbb2"   : {'label': r"$p_{T}\text{(bb from H2)\ [GeV]}$", 'range': np.linspace(0, 500, 41)},
             "etabb1"  : {'label': r"$\eta\text{(bb from H1)}$", 'range': np.linspace(-5, 5, 41)},
             "etabb2"  : {'label': r"$\eta\text{(bb from H2)}$", 'range': np.linspace(-5, 5, 41)},

             "pth1"    : {'label': r"$p_{T}\text{(H1) [GeV]}$", 'range': np.linspace(0, 500, 41)},
             "pth2"    : {'label': r"$p_{T}\text{(H2) [GeV]}$", 'range': np.linspace(0, 500, 41)},
             "etah1"   : {'label': r"$\eta\text{(H1)}$", 'range': np.linspace(-5, 5, 41)},
             "etah2"   : {'label': r"$\eta\text{(H2)}$", 'range': np.linspace(-5, 5, 41)},

             "ptb1"    : {'label': r"$p_{T}\text{(b1) [GeV]}$", 'range': np.linspace(0, 500, 41)},
             "ptb2"    : {'label': r"$p_{T}\text{(b2) [GeV]}$", 'range': np.linspace(0, 500, 41)},
             "ptb3"    : {'label': r"$p_{T}\text{(b3) [GeV]}$", 'range': np.linspace(0, 500, 41)},
             "ptb4"    : {'label': r"$p_{T}\text{(b4) [GeV]}$", 'range': np.linspace(0, 500, 41)},

             "dphibb1" : {'label': r"$\Delta\Phi\text{(bb from H1)}$", 'range': np.linspace(-3.14159265359, 3.14159265359, 41)},
             "dphibb2" : {'label': r"$\Delta\Phi\text{(bb from H2)}$", 'range': np.linspace(-3.14159265359, 3.14159265359, 41)},
             "dphihh"  : {'label': r"$\Delta\Phi\text{(HH)}$", 'range': np.linspace(-3.14159265359, 3.14159265359, 41)},
             "drbb1"   : {'label': r"$\Delta R\text{(bb from H1)}$", 'range': np.linspace(0, 6, 41)},
             "drbb2"   : {'label': r"$\Delta R\text{(bb from H2)}$", 'range': np.linspace(0, 6, 41)},
             "drhh"    : {'label': r"$\Delta R\text{(HH)}$", 'range': np.linspace(0, 6, 41)},
             
             "njet"    : {'label': r"$\text{Number of jets}$", 'range': np.linspace(0, 20, 21)},
             "ptjet"   : {'label': r"$p_{T}\text{(jet) [GeV]}$", 'range': np.linspace(0, 500, 41)}}

ftmp = ROOT.TFile(f"kl__{kl}.root","RECREATE")

evt = ROOT.POOL.TEvent(ROOT.POOL.TEvent.kClassAccess)
status = evt.readFrom(inputs[kl])
evt.getEntry(0)
eventInfo = evt.retrieve("xAOD::EventInfo", "EventInfo")
print(f"kl={kl} mcEventWeights={eventInfo.mcEventWeights()[0]} status={status.getCode()} entries={evt.getEntries()}")

ftmp.cd()
hists = {}
for va in variables.keys():
    hists[va] = ROOT.TH1F(va, va, len(variables[va]['range'])-1, variables[va]['range'])

for i in range(0,evt.getEntries()):
    evt.getEntry(i)
    higgs_start = []
    higgs_end = []
    eventInfo = evt.retrieve("xAOD::EventInfo", "EventInfo")
    weight = eventInfo.mcEventWeights()[0]/evt.getEntries()

    jets = evt.retrieve("xAOD::JetContainer","AntiKt4TruthDressedWZJets")
    j_pts = []
    for j in jets:
        if j.pt() < 20e3: continue
        j_pts.append(j.pt())
    
    particles = evt.retrieve("xAOD::TruthParticleContainer","TruthBosonsWithDecayParticles")
    for p in particles:
        if not p.isHiggs(): continue
        start = True
        for pp in range(p.nParents()):
            if p.parent(pp).isHiggs():
                start = False
        end = True
        for pp in range(p.nChildren()):
            if p.child(pp).isHiggs():
                end = False
        if start: higgs_start.append(p)
        if end: higgs_end.append(p)
    if len(higgs_start) != 2 or len(higgs_end) != 2:
        raise Exception("ERROR Higgs candidates")
    
    if higgs_start[0].pt() > higgs_start[1].pt():
        h1 = higgs_start[0].p4()
        h2 = higgs_start[1].p4()
    else:
        h1 = higgs_start[1].p4()
        h2 = higgs_start[0].p4()
    if higgs_end[0].pt() > higgs_end[1].pt():
        hbb1 = higgs_end[0]
        hbb2 = higgs_end[1]
    else:
        hbb1 = higgs_end[1]
        hbb2 = higgs_end[0]
    if hbb1.nChildren() != 2 or hbb2.nChildren() != 2:
        raise Exception("ERROR unexpected decay")
    for pp in range(2):
        if abs(hbb1.child(pp).pdgId()) != 5 or abs(hbb2.child(pp).pdgId()) != 5:
            raise Exception("ERROR unexpected decay")
    
    if hbb1.child(0).pt() > hbb1.child(1).pt():
        hbb1_b1 = hbb1.child(0).p4()
        hbb1_b2 = hbb1.child(1).p4()
    else:
        hbb1_b1 = hbb1.child(1).p4()
        hbb1_b2 = hbb1.child(0).p4()
    if hbb2.child(0).pt() > hbb2.child(1).pt():
        hbb2_b1 = hbb2.child(0).p4()
        hbb2_b2 = hbb2.child(1).p4()
    else:
        hbb2_b1 = hbb2.child(1).p4()
        hbb2_b2 = hbb2.child(0).p4()
    b_pts = [hbb1_b1.Pt(), hbb1_b2.Pt(), hbb2_b1.Pt(), hbb2_b2.Pt()]
    b_pts.sort(reverse=True)
    
    for va in variables.keys():
        if va=='mhh':
            safe_fill(hists[va], (h1+h2).M()/1e3, weight)
        elif va == 'mbbbb':
            safe_fill(hists[va], (hbb1_b1+hbb1_b2+hbb2_b1+hbb2_b2).M()/1e3, weight)
        elif va == 'mbb1':
            safe_fill(hists[va], (hbb1_b1+hbb1_b2).M()/1e3, weight)
        elif va == 'mbb2':
            safe_fill(hists[va], (hbb2_b1+hbb2_b2).M()/1e3, weight)
        elif va == 'ptbb1':
            safe_fill(hists[va], (hbb1_b1+hbb1_b2).Pt()/1e3, weight)
        elif va == 'ptbb2':
            safe_fill(hists[va], (hbb2_b1+hbb2_b2).Pt()/1e3, weight)
        elif va == 'etabb1':
            safe_fill(hists[va], (hbb1_b1+hbb1_b2).Eta(), weight)
        elif va == 'etabb2':
            safe_fill(hists[va], (hbb2_b1+hbb2_b2).Eta(), weight)
        elif va == 'pth1':
            safe_fill(hists[va], h1.Pt()/1e3, weight)
        elif va == 'pth2':
            safe_fill(hists[va], h2.Pt()/1e3, weight)
        elif va == 'etah1':
            safe_fill(hists[va], h1.Eta(), weight)
        elif va == 'etah2':
            safe_fill(hists[va], h2.Eta(), weight)
        elif va == 'ptb1':
            safe_fill(hists[va], b_pts[0]/1e3, weight)
        elif va == 'ptb2':
            safe_fill(hists[va], b_pts[1]/1e3, weight)
        elif va == 'ptb3':
            safe_fill(hists[va], b_pts[2]/1e3, weight)
        elif va == 'ptb4':
            safe_fill(hists[va], b_pts[3]/1e3, weight)
        elif va == 'dphibb1':
            safe_fill(hists[va], hbb1_b1.DeltaPhi(hbb1_b2), weight)
        elif va == 'dphibb2':
            safe_fill(hists[va], hbb2_b1.DeltaPhi(hbb2_b2), weight)
        elif va == 'dphihh':
            safe_fill(hists[va], h1.DeltaPhi(h2), weight)
        elif va == 'drbb1':
            safe_fill(hists[va], hbb1_b1.DeltaR(hbb1_b2), weight)
        elif va == 'drbb2':
            safe_fill(hists[va], hbb2_b1.DeltaR(hbb2_b2), weight)
        elif va == 'drhh':
            safe_fill(hists[va], h1.DeltaR(h2), weight)
        elif va == 'njet':
            safe_fill(hists[va], len(j_pts), weight)
        elif va == 'ptjet':
            for pt in j_pts:
                safe_fill(hists[va], pt/1e3, weight)
ftmp.cd()
for va in variables.keys():
    hists[va].Write()

# import atlasplots as aplt
# aplt.set_atlas_style()
# colors = [ROOT.kBlack, ROOT.kRed+1, ROOT.kAzure+1]
# for va in variables.keys():
#     fig, ax = aplt.subplots(1, 1, name=va, figsize=(800, 600))
#     for i, kl in enumerate(group):
#         ax.plot(all_hists[kl][va], label=f"kl={kl}", labelfmt="L", linecolor=colors[i])
#         ax.plot(aplt.root_helpers.hist_to_graph(all_hists[kl][va]),"2", fillcolor=colors[i], linecolor=colors[i])
#     ax.add_margins(top=0.15)
#     ax.set_xlabel(variables[va]['label'])
#     ax.set_ylabel("Events")
#     ax.legend(loc=(0.7, 0.74, 0.95, 0.92))
#     fig.savefig(f"figures_vali/{va}.png")
