import ROOT
import numpy as np

inputs = {"-1":      "/afs/cern.ch/user/c/caiyi/eos/HH4b/production/upload/vali/000006/DAOD_TRUTH1.test.pool.root",
          "0":       "/afs/cern.ch/user/c/caiyi/eos/HH4b/production/upload/vali/000001/DAOD_TRUTH1.test.pool.root",
          "1":       "/afs/cern.ch/user/c/caiyi/eos/HH4b/production/upload/vali/000002/DAOD_TRUTH1.test.pool.root",
          "2.5":     "/afs/cern.ch/user/c/caiyi/eos/HH4b/production/upload/vali/000005/DAOD_TRUTH1.test.pool.root",
          "5":       "/afs/cern.ch/user/c/caiyi/eos/HH4b/production/upload/vali/000004/DAOD_TRUTH1.test.pool.root",
          "10":      "/afs/cern.ch/user/c/caiyi/eos/HH4b/production/upload/vali/000003/DAOD_TRUTH1.test.pool.root",
          "-1_mc20": "/afs/cern.ch/user/c/caiyi/eos/HH4b/production/upload/vali_mc20/000006/DAOD_TRUTH1.test.pool.root",
          "0_mc20":  "/afs/cern.ch/user/c/caiyi/eos/HH4b/production/upload/vali_mc20/000001/DAOD_TRUTH1.test.pool.root",
          "1_mc20":  "/afs/cern.ch/user/c/caiyi/eos/HH4b/production/upload/vali_mc20/000002/DAOD_TRUTH1.test.pool.root",
          "2.5_mc20":"/afs/cern.ch/user/c/caiyi/eos/HH4b/production/upload/vali_mc20/000005/DAOD_TRUTH1.test.pool.root",
          "5_mc20":  "/afs/cern.ch/user/c/caiyi/eos/HH4b/production/upload/vali_mc20/000004/DAOD_TRUTH1.test.pool.root",
          "10_mc20": "/afs/cern.ch/user/c/cawiyi/eos/HH4b/production/upload/vali_mc20/000003/DAOD_TRUTH1.test.pool.root",
          "1_mc16":   "/afs/cern.ch/user/c/caiyi/eos/HH4b/production/comp/mc16_13TeV.600463.PhPy8EG_PDF4LHC15_HH4b_cHHH01d0.merge.EVNT.e8222_e7400/DAOD_TRUTH1.test.DAOD.root",
          "10_mc16":  "/afs/cern.ch/user/c/caiyi/eos/HH4b/production/comp/mc16_13TeV.600464.PhPy8EG_PDF4LHC15_HH4b_cHHH10d0.merge.EVNT.e8222_e7400/DAOD_TRUTH1.test.DAOD.root",
          "1_mc21":  "/eos/user/l/lshi/Shared/hh4b_mc21/TRUTH1/kl1/DAOD_TRUTH1.PhPyHH.pool.root",
          "10_mc21": "/eos/user/l/lshi/Shared/hh4b_mc21/TRUTH1/kl10/DAOD_TRUTH1.PhPyHH.pool.root",}

group = [["1","1_mc21"],["10","10_mc21"]]; suffix = "mc23_mc21_"; ratiolabel = "mc23 / mc21"; label = ["mc23", "mc21"]
# group = [["1_mc20","1_mc16"],["10_mc20","10_mc16"]]; suffix = "mc20_mc16_"; ratiolabel = "mc20 / mc16"; label = ["mc20", "mc16"]
fall = {}
for kls in group:
    fall[kls[0]] = ROOT.TFile.Open(f"kl__{kls[0]}.root","READ")
    fall[kls[1]] = ROOT.TFile.Open(f"kl__{kls[1]}.root","READ")

variables = {"mhh"     : {'label': r"$m_{HH}\ \text{[GeV]}$", 'range': np.linspace(200, 1000, 41)},
             "mbbbb"   : {'label': r"$m_{bbbb}\ \text{[GeV]}$", 'range': np.linspace(200, 1000, 41)},
             "mbb1"    : {'label': r"$m_{\text{(bb from H1)}}\ \text{[GeV]}$", 'range': np.linspace(0, 200, 41)},
             "mbb2"    : {'label': r"$m_{\text{(bb from H2)}}\ \text{[GeV]}$", 'range': np.linspace(0, 200, 41)},
             
             "ptbb1"   : {'label': r"$p_{T}\text{(bb from H1)\ [GeV]}$", 'range': np.linspace(0, 500, 41)},
             "ptbb2"   : {'label': r"$p_{T}\text{(bb from H2)\ [GeV]}$", 'range': np.linspace(0, 500, 41)},
             "etabb1"  : {'label': r"$\eta\text{(bb from H1)}$", 'range': np.linspace(-5, 5, 41)},
             "etabb2"  : {'label': r"$\eta\text{(bb from H2)}$", 'range': np.linspace(-5, 5, 41)},

             "pth1"    : {'label': r"$p_{T}\text{(H1) [GeV]}$", 'range': np.linspace(0, 500, 41)},
             "pth2"    : {'label': r"$p_{T}\text{(H2) [GeV]}$", 'range': np.linspace(0, 500, 41)},
             "etah1"   : {'label': r"$\eta\text{(H1)}$", 'range': np.linspace(-5, 5, 41)},
             "etah2"   : {'label': r"$\eta\text{(H2)}$", 'range': np.linspace(-5, 5, 41)},

             "ptb1"    : {'label': r"$p_{T}\text{(b1) [GeV]}$", 'range': np.linspace(0, 500, 41)},
             "ptb2"    : {'label': r"$p_{T}\text{(b2) [GeV]}$", 'range': np.linspace(0, 500, 41)},
             "ptb3"    : {'label': r"$p_{T}\text{(b3) [GeV]}$", 'range': np.linspace(0, 500, 41)},
             "ptb4"    : {'label': r"$p_{T}\text{(b4) [GeV]}$", 'range': np.linspace(0, 500, 41)},

             "dphibb1" : {'label': r"$\Delta\Phi\text{(bb from H1)}$", 'range': np.linspace(-3.14159265359, 3.14159265359, 41)},
             "dphibb2" : {'label': r"$\Delta\Phi\text{(bb from H2)}$", 'range': np.linspace(-3.14159265359, 3.14159265359, 41)},
             "dphihh"  : {'label': r"$\Delta\Phi\text{(HH)}$", 'range': np.linspace(-3.14159265359, 3.14159265359, 41)},
             "drbb1"   : {'label': r"$\Delta R\text{(bb from H1)}$", 'range': np.linspace(0, 6, 41)},
             "drbb2"   : {'label': r"$\Delta R\text{(bb from H2)}$", 'range': np.linspace(0, 6, 41)},
             "drhh"    : {'label': r"$\Delta R\text{(HH)}$", 'range': np.linspace(0, 6, 41)},
             
             "njet"    : {'label': r"$\text{Number of jets}$", 'range': np.linspace(0, 20, 21)},
             "ptjet"   : {'label': r"$p_{T}\text{(jet) [GeV]}$", 'range': np.linspace(0, 500, 41)}}

import atlasplots as aplt
aplt.set_atlas_style()
colors = [ROOT.kRed+1, ROOT.kAzure+1]

for va in variables.keys():
    fig, (ax1, ax2) = aplt.ratio_plot(name=va, figsize=(800, 800), hspace=0.05)
    line = ROOT.TLine(variables[va]['range'][0], 1, variables[va]['range'][-1], 1)
    ax2.plot(line, linecolor=ROOT.kBlack, linewidth=1)
    hgs = {}
    for i, kls in enumerate(group):
        h = fall[kls[0]].Get(va)
        hold = fall[kls[1]].Get(va)
        kl = kls[0]
        # inte = h.Integral()
        h.Scale(1/h.Integral())
        kln = kls[0].split("_")[0]
        ax1.plot(h, label=f"kl={kln} {label[0]}", labelfmt="L", linecolor=colors[i],linewidth=2)
        hgs[kl] = aplt.root_helpers.hist_to_graph(h)
        ax1.plot(hgs[kl], "2", fillcolor=colors[i], linecolor=colors[i], linewidth=2)
        hold.Scale(1/hold.Integral())
        ax1.plot(hold, label=f"kl={kln} {label[1]}", labelfmt="L", linecolor=colors[i], linestyle=2, linewidth=2)
        hgs[kl+"ratio"] = h.Clone(kl+"ratio")
        hgs[kl+"ratio"].Divide(hold)
        hgs[kl+"ratio_graph"] = aplt.root_helpers.hist_to_graph(hgs[kl+"ratio"],show_bin_width=True)
        # ax2.plot(hgs[kl+"ratio_graph"], "0", fillcolor=colors[i], linecolor=colors[i], linewidth=2)
        ax2.plot(hgs[kl+"ratio_graph"], "EP", fillcolor=colors[i], linecolor=colors[i], markercolor=colors[i])
        ax2.set_ylim(0.6, 1.4)
        ax2.draw_arrows_outside_range(hgs[kl+"ratio_graph"],fillcolor=colors[i],linecolor=colors[i])
    ax1.add_margins(top=0.15)
    ax1.set_ylabel("")
    ax2.set_ylabel(ratiolabel)
    ax2.text(0.7, 0.1, variables[va]['label'], size=26)
    if 'oldPDF' in suffix:
        ax1.legend(loc=(0.55, 0.7, 0.95, 0.92))
    else:
        ax1.legend(loc=(0.65, 0.7, 0.95, 0.92))
    fig.savefig(f"figures_comp/{suffix}{va}.png")
