from lhe_reader import LHEDataFrame
import sys
import numpy as np
import ROOT
ROOT.gStyle.SetOptStat(0)
import os
os.makedirs('figures', exist_ok=True)

def create_root_histogram(data, weight, label, x_label, bins, min_bin, max_bin, save_path):
    # Create a ROOT histogram for the data
    hist = ROOT.TH1F(x_label, "", bins, min_bin, max_bin)
    for i, value in enumerate(data):
        hist.Fill(value, weight[i])
    hist.Scale(1/len(data))
    
    # Create canvas and draw histogram
    canvas = ROOT.TCanvas("Canvas", "Canvas", 800, 600)
    hist.Draw("H E")
    
    # Set axis labels
    hist.GetXaxis().SetTitle(x_label)
    hist.GetYaxis().SetTitle("Events")

    legend = ROOT.TLegend(0.6,0.65,0.8,0.85)
    legend.SetBorderSize(0)
    legend.AddEntry(hist, label, "l")
    legend.Draw()

    canvas.Print(save_path)

def pt(px, py):
    return np.sqrt(px**2 + py**2)

def calculate_invariant_mass(events):
    invariant_masses = []
    delta_phis = []
    pt1s = []
    pt2s = []
    weights = []
    for event in events:
        weights.append(event['event_weight'])

        higgs_bosons = [p for p in event['particles'] if p['id'] == 25]  # PDG ID for Higgs boson is 25
        if len(higgs_bosons) >= 2:
            # Calculate invariant mass for the first two Higgs bosons found
            E = higgs_bosons[0]['energy'] + higgs_bosons[1]['energy']
            px = higgs_bosons[0]['px'] + higgs_bosons[1]['px']
            py = higgs_bosons[0]['py'] + higgs_bosons[1]['py']
            pz = higgs_bosons[0]['pz'] + higgs_bosons[1]['pz']
            m_squared = E**2 - (px**2 + py**2 + pz**2)
            if m_squared > 0:  # Ensure positive before taking the square root
                invariant_masses.append(np.sqrt(m_squared))

            phi1 = np.arctan2(higgs_bosons[0]['py'], higgs_bosons[0]['px'])
            phi2 = np.arctan2(higgs_bosons[1]['py'], higgs_bosons[1]['px'])
            dphi = phi2 - phi1
            delta_phis.append(np.arctan2(np.sin(dphi), np.cos(dphi)))
            
            pt1 = pt(higgs_bosons[0]['px'], higgs_bosons[0]['py'])
            pt2 = pt(higgs_bosons[1]['px'], higgs_bosons[1]['py'])
            pt1s.append(max(pt1, pt2))
            pt2s.append(min(pt1, pt2))

    return invariant_masses, delta_phis, pt1s, pt2s, weights

if sys.argv[1:]:
    name = sys.argv[1]
else:
    name = 'ggF_HH_chhh1p0'

filepath = '../' + name + '/test.TXT.events'
lhe_data = LHEDataFrame(filepath)
events = lhe_data.get_events()

# Calculate invariant masses
invariant_masses, delta_phis, pt1s, pt2s, weights = calculate_invariant_mass(events)

# Plot histogram
label = 'kl = ' + name.split('_')[-1].replace('p','.').replace('n','-').replace('chhh','')

create_root_histogram(invariant_masses, weights, label, "m_{HH} [GeV]", 50, 240, 1000, "figures/mtt_" + name + ".png")
create_root_histogram(delta_phis, weights, label, "#Delta#Phi_{HH}", 50, -ROOT.TMath.Pi(), ROOT.TMath.Pi(), "figures/dphi_" + name + ".png")
create_root_histogram(pt1s, weights, label, "p_{T} (leading H) [GeV]", 50, 0, 600, "figures/pt1_" + name + ".png")
create_root_histogram(pt2s, weights, label, "p_{T} (sub-leading H) [GeV]", 50, 0, 600, "figures/pt2_" + name + ".png")
