Commands to run:

```
setupATLAS
asetup AthGeneration,23.6.28,here

cd ggF_HH_chhh0p0
unset ATHENA_PROC_NUMBER

Gen_tf.py --ecmEnergy=13600 --randomSeed=12345 --firstEvent=1 --maxEvents=400 --jobConfig=./ --outputTXTFile=test.TXT.tar.gz
```
