import ROOT
import numpy as np

inputs = {"-1":      "/afs/cern.ch/user/c/caiyi/eos/HH4b/production/upload/vali/000006/DAOD_TRUTH1.test.pool.root",
          "0":       "/afs/cern.ch/user/c/caiyi/eos/HH4b/production/upload/vali/000001/DAOD_TRUTH1.test.pool.root",
          "1":       "/afs/cern.ch/user/c/caiyi/eos/HH4b/production/upload/vali/000002/DAOD_TRUTH1.test.pool.root",
          "2.5":     "/afs/cern.ch/user/c/caiyi/eos/HH4b/production/upload/vali/000005/DAOD_TRUTH1.test.pool.root",
          "5":       "/afs/cern.ch/user/c/caiyi/eos/HH4b/production/upload/vali/000004/DAOD_TRUTH1.test.pool.root",
          "10":      "/afs/cern.ch/user/c/caiyi/eos/HH4b/production/upload/vali/000003/DAOD_TRUTH1.test.pool.root",
          "-1_mc20": "/afs/cern.ch/user/c/caiyi/eos/HH4b/production/upload/vali_mc20/000006/DAOD_TRUTH1.test.pool.root",
          "0_mc20":  "/afs/cern.ch/user/c/caiyi/eos/HH4b/production/upload/vali_mc20/000001/DAOD_TRUTH1.test.pool.root",
          "1_mc20":  "/afs/cern.ch/user/c/caiyi/eos/HH4b/production/upload/vali_mc20/000002/DAOD_TRUTH1.test.pool.root",
          "2.5_mc20":"/afs/cern.ch/user/c/caiyi/eos/HH4b/production/upload/vali_mc20/000005/DAOD_TRUTH1.test.pool.root",
          "5_mc20":  "/afs/cern.ch/user/c/caiyi/eos/HH4b/production/upload/vali_mc20/000004/DAOD_TRUTH1.test.pool.root",
          "10_mc20": "/afs/cern.ch/user/c/cawiyi/eos/HH4b/production/upload/vali_mc20/000003/DAOD_TRUTH1.test.pool.root",
          "1_mc16":   "/afs/cern.ch/user/c/caiyi/eos/HH4b/production/comp/mc16_13TeV.600463.PhPy8EG_PDF4LHC15_HH4b_cHHH01d0.merge.EVNT.e8222_e7400/DAOD_TRUTH1.test.DAOD.root",
          "10_mc16":  "/afs/cern.ch/user/c/caiyi/eos/HH4b/production/comp/mc16_13TeV.600464.PhPy8EG_PDF4LHC15_HH4b_cHHH10d0.merge.EVNT.e8222_e7400/DAOD_TRUTH1.test.DAOD.root",
          "1_mc21":  "/eos/user/l/lshi/Shared/hh4b_mc21/TRUTH1/kl1/DAOD_TRUTH1.PhPyHH.pool.root",
          "10_mc21": "/eos/user/l/lshi/Shared/hh4b_mc21/TRUTH1/kl10/DAOD_TRUTH1.PhPyHH.pool.root",}

groups = {"nom": ["0","1","5"], "val": ["-1","2.5","10"], "nom_mc20": ["0_mc20","1_mc20","5_mc20"], "val_mc20": ["-1_mc20","2.5_mc20","10_mc20"]}

for groupkey in groups.keys():
    group = groups[groupkey]
    suffix = "" if 'nom' in groupkey else "val_"
    mc20 = "_mc20" if "mc20" in groupkey else ""
    fall = {}
    for kl in group:
        fall[kl] = ROOT.TFile.Open(f"kl__{kl}.root","READ")
        
    variables = {"mhh"     : {'label': r"$m_{HH}\ \text{[GeV]}$", 'range': np.linspace(200, 1000, 41)},
                "mbbbb"   : {'label': r"$m_{bbbb}\ \text{[GeV]}$", 'range': np.linspace(200, 1000, 41)},
                "mbb1"    : {'label': r"$m_{\text{(bb from H1)}}\ \text{[GeV]}$", 'range': np.linspace(0, 200, 41)},
                "mbb2"    : {'label': r"$m_{\text{(bb from H2)}}\ \text{[GeV]}$", 'range': np.linspace(0, 200, 41)},
                
                "ptbb1"   : {'label': r"$p_{T}\text{(bb from H1)\ [GeV]}$", 'range': np.linspace(0, 500, 41)},
                "ptbb2"   : {'label': r"$p_{T}\text{(bb from H2)\ [GeV]}$", 'range': np.linspace(0, 500, 41)},
                "etabb1"  : {'label': r"$\eta\text{(bb from H1)}$", 'range': np.linspace(-5, 5, 41)},
                "etabb2"  : {'label': r"$\eta\text{(bb from H2)}$", 'range': np.linspace(-5, 5, 41)},

                "pth1"    : {'label': r"$p_{T}\text{(H1) [GeV]}$", 'range': np.linspace(0, 500, 41)},
                "pth2"    : {'label': r"$p_{T}\text{(H2) [GeV]}$", 'range': np.linspace(0, 500, 41)},
                "etah1"   : {'label': r"$\eta\text{(H1)}$", 'range': np.linspace(-5, 5, 41)},
                "etah2"   : {'label': r"$\eta\text{(H2)}$", 'range': np.linspace(-5, 5, 41)},

                "ptb1"    : {'label': r"$p_{T}\text{(b1) [GeV]}$", 'range': np.linspace(0, 500, 41)},
                "ptb2"    : {'label': r"$p_{T}\text{(b2) [GeV]}$", 'range': np.linspace(0, 500, 41)},
                "ptb3"    : {'label': r"$p_{T}\text{(b3) [GeV]}$", 'range': np.linspace(0, 500, 41)},
                "ptb4"    : {'label': r"$p_{T}\text{(b4) [GeV]}$", 'range': np.linspace(0, 500, 41)},

                "dphibb1" : {'label': r"$\Delta\Phi\text{(bb from H1)}$", 'range': np.linspace(-3.14159265359, 3.14159265359, 41)},
                "dphibb2" : {'label': r"$\Delta\Phi\text{(bb from H2)}$", 'range': np.linspace(-3.14159265359, 3.14159265359, 41)},
                "dphihh"  : {'label': r"$\Delta\Phi\text{(HH)}$", 'range': np.linspace(-3.14159265359, 3.14159265359, 41)},
                "drbb1"   : {'label': r"$\Delta R\text{(bb from H1)}$", 'range': np.linspace(0, 6, 41)},
                "drbb2"   : {'label': r"$\Delta R\text{(bb from H2)}$", 'range': np.linspace(0, 6, 41)},
                "drhh"    : {'label': r"$\Delta R\text{(HH)}$", 'range': np.linspace(0, 6, 41)},
                
                "njet"    : {'label': r"$\text{Number of jets}$", 'range': np.linspace(0, 20, 21)},
                "ptjet"   : {'label': r"$p_{T}\text{(jet) [GeV]}$", 'range': np.linspace(0, 500, 41)}}

    import atlasplots as aplt
    aplt.set_atlas_style()
    colors = [ROOT.kBlack, ROOT.kRed+1, ROOT.kAzure+1]

    for va in variables.keys():
        fig, ax = aplt.subplots(1, 1, name=va, figsize=(800, 600))
        hgs = {}
        for i, kl in enumerate(group):
            h = fall[kl].Get(va)
            inte = h.Integral()
            h.Scale(1/inte)
            kln = kl.split("_")[0]
            if "jet" in va:
                ax.plot(h, label=f"kl={kln}", labelfmt="L", linecolor=colors[i],linewidth=2)
            else:
                ax.plot(h, label=f"kl={kln} / {inte:.3f}", labelfmt="L", linecolor=colors[i],linewidth=2)
            hgs[kl] = aplt.root_helpers.hist_to_graph(h)
            ax.plot(hgs[kl], "2", fillcolor=colors[i], linecolor=colors[i], linewidth=2)
        ax.add_margins(top=0.15)
        ax.set_xlabel(variables[va]['label'])
        ax.set_ylabel("")
        ax.legend(loc=(0.65, 0.74, 0.95, 0.92))
        fig.savefig(f"figures_vali{mc20}/{suffix}{va}.png")
