#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description    = "diHiggs production with chhh=-1.0 using Powheg-Box-V2 at NLO + full top mass, with PDF4LHC21 PDF. LHE-only production."
evgenConfig.keywords       = ["hh", "SM", "SMHiggs", "nonResonant"]
evgenConfig.contact = [ "tpelzer@cern.ch" ]
evgenConfig.generators += [ "Powheg" ]
evgenConfig.nEventsPerJob  = 400

# --------------------------------------------------------------
# Configuring Powheg
# --------------------------------------------------------------

### Load ATLAS defaults for the Powheg ggF_HH process
include("PowhegControl/PowhegControl_ggF_HH_Common.py")

### important parameters
PowhegConfig.mtdep = 3 # full theory
PowhegConfig.mass_H = 125 # Higgs boson mass - need to stick to that value as it is hardcoded in the ME
PowhegConfig.mass_t = 173 # top-quark mass - need to stick to that value as it is hardcoded in the ME
PowhegConfig.hdamp = 250

### Modify coupling
PowhegConfig.chhh = -1.0 # Trilinear Higgs self-coupling [default: 1.0 (SM)]

### scales and PDF sets
PowhegConfig.PDF = list(range(93300,93343)) # PDF4LHC21_40_pdfas with error sets
PowhegConfig.PDF += list(range(325300,325403)) # NNPDF30_nnlo_as_0118 with error sets
PowhegConfig.PDF += [27100,14400,331700] # MSHT20nlo_as118, CT18NLO, NNPDF40_nlo_as_01180 nominal sets
PowhegConfig.mu_F = [ 1.0, 0.5, 0.5, 2.0, 2.0, 1.0, 1.0]
PowhegConfig.mu_R = [ 1.0, 0.5, 1.0, 1.0, 2.0, 0.5, 2.0]

### Generate events
PowhegConfig.generate()
