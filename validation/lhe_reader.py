import pandas as pd
import tarfile
import os
import io

class LHEDataFrame:
    def __init__(self, filepath):
        self.filepath = filepath
        self.init_data = {}
        self.events = []
        self._load_data()

    def _extract_from_targz(self, tar_path):
        with tarfile.open(tar_path, "r:gz") as tar:
            lhe_member = [m for m in tar.getmembers() if m.name.endswith('.lhe')][0]
            extracted_file = tar.extractfile(lhe_member)
            return extracted_file.read().decode('utf-8')

    def _parse_init(self, text):
        init_start = text.find('<init>')
        init_end = text.find('</init>', init_start)
        init_section = text[init_start:init_end].split('\n')[1:-1]  # Exclude <init> and </init> tags
        
        # Example of processing the first line of the <init> section for demonstration
        # You should adjust the processing logic based on the actual data structure of your LHE files
        if init_section:
            init_data = init_section[0].split()
            self.init_data = {
                'beam_particle_ids': (int(init_data[0]), int(init_data[1])),
                'beam_energies': (float(init_data[2]), float(init_data[3])),
                # Add more fields as necessary
            }

    def _parse_events(self, text):
        events = text.split('<event>')
        for event in events[1:]:  # Skip the first split part, which is not an event
            event_end = event.find('</event>')
            event_content = event[:event_end].strip().split('\n')
            
            event_header = event_content[0].split()
            event_dict = {
                'num_particles': int(event_header[0]),
                'event_weight': float(event_header[2]),
                'particles': [],
                'rwgt': {}
            }

            # rwgt_start = event.find('<rwgt>')
            # rwgt_end = event.find('</rwgt>')
            # if rwgt_start != -1 and rwgt_end != -1:
            #     rwgt_section = event[rwgt_start:rwgt_end].split('\n')[1:-1]  # Exclude <rwgt> and </rwgt> tags
            #     for rwgt_entry in rwgt_section:
            #         rwgt_data = rwgt_entry.split('\'')
            #         event_dict['rwgt'][rwgt_data[1]] = float(rwgt_data[2].replace('</wgt>','').replace('>', ''))

            for line in event_content[1:]:
                if line.startswith('#') or 'wgt' in line:  # Skip comments
                    continue
                else:
                    particle_data = line.split()
                    if len(particle_data) > 11:  # Ensure it's a valid particle data line
                        particle_dict = {
                            'id': int(particle_data[0]),
                            'status': int(particle_data[1]),
                            'mother1': int(particle_data[2]),
                            'mother2': int(particle_data[3]),
                            'px': float(particle_data[6]),
                            'py': float(particle_data[7]),
                            'pz': float(particle_data[8]),
                            'energy': float(particle_data[9]),
                            'mass': float(particle_data[10]),
                            # Add more fields as necessary
                        }
                        event_dict['particles'].append(particle_dict)

            self.events.append(event_dict)
    
    def _load_data(self):
        if self.filepath.endswith('.tar.gz'):
            file_content = self._extract_from_targz(self.filepath)
        else:
            with open(self.filepath, 'r') as file:
                file_content = file.read()

        self._parse_init(file_content)
        self._parse_events(file_content)

    def get_init_data(self):
        return self.init_data

    def get_events(self):
        return self.events
