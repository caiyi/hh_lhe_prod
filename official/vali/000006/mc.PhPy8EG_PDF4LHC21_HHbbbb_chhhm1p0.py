#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description    = "diHiggs production, decay to bbbb, with Powheg-Box-V2, at NLO + full top mass, with PDF4LHC21 PDF, PS&had with Pythia8 with A14 NNPDF2.3 tune."
evgenConfig.keywords       = ["hh", "SM", "SMHiggs", "nonResonant", "bbbar","bottom"]
evgenConfig.contact = ["yizhou.cai@cern.ch"]
evgenConfig.nEventsPerJob  = 10000

# --------------------------------------------------------------
# Configuring Powheg
# --------------------------------------------------------------

### Load ATLAS defaults for the Powheg ggF_HH process
include("PowhegControl/PowhegControl_ggF_HH_Common.py")

PowhegConfig.hdamp = 250
### Modify couplings
PowhegConfig.chhh = -1.0 # Trilinear Higgs self-coupling [default: 1.0 (SM)]

### scales and PDF sets
PowhegConfig.PDF = 93300 
PowhegConfig.mu_F = 1.0 
PowhegConfig.mu_R = 1.0 

### Generate events
PowhegConfig.generate()

#--------------------------------------------------------------
# Configuring Pythia8
#--------------------------------------------------------------

### Pythia8 showering with the A14 NNPDF2.3 tune, main31 routine
include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_Powheg.py")
### main31 routine for showering Powheg events with Powheg
include("Pythia8_i/Pythia8_Powheg_Main31.py")

### selectin Higgs decays
genSeq.Pythia8.Commands += [ "25:oneChannel = on 0.5 100 5 -5 ",  # bb decay
                             "25:addChannel = on 0.5 100 5 -5 ", # bb decay
                             "TimeShower:mMaxGamma = 0" ] # Z/gamma* combination scale



